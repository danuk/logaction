PREFIX=/opt
CC = gcc 
CFLAGS = -D_FILE_OFFSET_BITS=64 -ggdb
LIBS = `pcre-config --libs` 

all: logaction
	strip -s logaction
	rm -f *.o

logaction.o : src/logaction.c src/logaction.h
	$(CC) -c src/logaction.c $(CFLAGS)

logaction : logaction.o
	$(CC) $(CFLAGS) -o logaction logaction.o $(LIBS)

install:
	mkdir -p $(PREFIX)/logaction/bin
	mkdir -p $(PREFIX)/logaction/etc
	mkdir -p $(PREFIX)/logaction/conf
	mkdir -p $(PREFIX)/logaction/scripts
	cp logaction $(PREFIX)/logaction/bin/
	cp etc/logaction.sh $(PREFIX)/logaction/etc/
	test -f $(PREFIX)/logaction/conf/logaction.cfg || cp conf/logaction.cfg $(PREFIX)/logaction/conf/

install_systemd:
	cp systemd/logaction.service /etc/systemd/system/
	systemctl enable logaction

clean : 
	rm -f *.o *~ logaction

