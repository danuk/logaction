
/*
 * Default config file
 * This parameter redirected by -c flag in command line
 */
#define CONFIG_FILE_NAME "logaction.cfg"

/*
 * Max available count of sections for porcessed config file
 * Max process for fork()
 */
#define MAX_SECTION_COUNT 16

/*
 * Max parameter for file: argv
 * For example: test.sh param1 param2 param3
 * - 3 param needed, 
 * also 3 parameter needed for REGEXP: /(\d+) (\w+)/ (one parameter used for all match REGEXP)
 * Note: Variable max_params_count used in both functions.
 */
#define MAX_PARAMS_COUNT 16

/*
 * Max rules count in one section
 * If count of rules in one sections exceed this value
 * then these rules be skiped
 */
#define MAX_RULES_COUNT 255

/*
 * This variable defined max length of string in log file.
 */
#define MAX_FILE_STRING_LENGTH 1024

/* PCRE NOTES::
	i  for PCRE_CASELESS
	m  for PCRE_MULTILINE
	s  for PCRE_DOTALL
	x  for PCRE_EXTENDED
	U  for PCRE_UNGREEDY
*/

/*
 * section_pcre - Perl Regular Expression
 * for searching section in config file
*/
#define SECTION_PCRE "(?Ums)^\\s*([A-Za-z0-9._/ -]+)\\s*{(.+)}\\s*$"

/*
 * For get one rule sting
 * This PCRE used in loop for processing each rule
*/
#define RULE_PCRE "(?m)^\\s*(.+)$"

/*
 * logaction_pcre - Perl Compatible Regular Expression
 * for searching logaction params:
 * First parameter defined the name of variable
 * Second parameter defined the PCRE expression for log file string
 * Last parameters defined script name for running and him parameters
*/
#define LOGACTION_PCRE "^logaction\\s+/(.+)/\\s+\"(.+)\"\\s*(\\[(\\d+),(\\d+)\\])?\\s*$"
//(\\[(\\d+),(\\d+)\\])?
#define INTERVAL_PCRE "(?i)^\\s*interval\\s*(\\d+)\\s*$"

#define LOGACTION	"logaction"
#define DEBUG		"debug"
#define INTERVAL	"interval"
/*
 * Default rule debug. Default is 0 
*/
#define DEF_RULE_DEBUG	0
#define DEF_INTERVAL 2

struct struct_rules
{
	char * log_file_name;
	int debug;
	int interval;
	struct struct_action * action;
};

struct struct_action
{
	pcre * regexp;
	pcre_extra * regexp_ext;
	char * file;
	char * params;
	int max_matchers;
	long int time_matcher;
	struct struct_action * next;
	struct matcher * matcher;
};

struct matcher
{
	char * str;
	int cur_count;
	long int time;
	struct matcher * next;
};


