// Written by DaNuk (DNk) 26-07-07

#include <stdio.h>
#include <pcre.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>
#include <time.h>

#include "logaction.h"

void sig_chld(int signo);

int debug=0;
int exec_pcre(char * string, char * regexp, int * array, pcre * f, pcre_extra * f_ext);
char * read_config_file(char * config_file);
char * get_first_param(char * str);
char * get_params_string(char * str);
void free_sections(int * sections, int count);
void set_param_logaction(struct struct_rules * rules, char * line);
void start_logaction(struct struct_rules * logaction);
void match_pcre (struct struct_action * action, char * str);
int add_matcher(struct struct_action * action, char * str);

pcre * compile_pcre(char * regexp);
struct struct_rules * parse_section(char * section_data);

int main(int argc, char* argv[])
{
	char * config_file = CONFIG_FILE_NAME;
	extern char *optarg;

	int c;
	while( (c=getopt(argc,argv,"c:hd")) != -1)
	{
		switch(c)
		{
		case 'c':
			config_file = optarg;
			break;
		case 'd':
			debug=1;
			break;
		case 'h':
			printf("Usage: %s\n",argv[0]);
			printf("  -c <Config file>  Config file\n");
			printf("  -d                Debug mode (only for first section)\n");
			printf("For more information visit home page: http://logaction.org\n");
			exit(1);
		}
	}
	
	struct sigaction sa;
	sa.sa_handler = &sig_chld;
	sa.sa_flags = SA_NOCLDWAIT;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGCHLD, &sa, NULL);

	char * config_data = read_config_file(config_file);

	struct struct_rules * logaction_rules;
	char * log_file_name;
	char * section_data;

	// Each sections must been have two params:
	// - Section name e.q. log_file_name
	// - Section data
	int sections[MAX_SECTION_COUNT];
	int count_sections = exec_pcre(config_data,SECTION_PCRE,sections,0,0);
	int i;

	if (debug) printf("Found sections count: %d\n",count_sections);
	
	for (i=1;i<count_sections;i+=3)
	{		
		log_file_name = (char*)sections[i];
		section_data = (char*)sections[i+1];
		
		logaction_rules = (struct struct_rules*) parse_section(section_data);
		if (logaction_rules == 0) break;
		
		logaction_rules->log_file_name = log_file_name;
	
		if (debug)
			printf("Started logaction for %s file\n",logaction_rules->log_file_name);
	
		start_logaction(logaction_rules);
	}	
	free_sections(sections,count_sections);
	free(config_data);	
		
	return 0;	
}

char * read_config_file(char * config_file)
{
	if (debug) printf("Read config file: %s ",config_file);
	
	struct stat *buf;
	buf = (struct stat*) malloc(sizeof(struct stat));
	
	int ret = stat(config_file,buf);
	
	if (ret!=0)
	{
		fprintf(stderr,"Can't stat config file: %s\n",config_file);
		free(buf);
		exit(1);
	}
	
	int config_size = buf->st_size;
	free(buf);	

	int fd = open(config_file,O_RDONLY);

	if (!fd) {fprintf(stderr,"Can't open config file: %s\n",config_file); exit(1);};
	
	char * config_data = malloc(sizeof(char)*config_size+1); 
	config_data[config_size]=0;
	
       	int n = read(fd, config_data, config_size);
	close(fd);

	if (!n)
	{
		fprintf(stderr,"Can't read config file: %s\n",config_file);
		exit(1);
	}	

	if (debug) printf("done\n");
	return config_data;
}

struct struct_rules * parse_section(char * section_data)
{	
	int line[MAX_RULES_COUNT];
	int lines = exec_pcre(section_data,RULE_PCRE,line,0,0);	
	int i;

	struct struct_rules * rules;
       	rules = (struct struct_rules*) malloc(sizeof(struct struct_rules));
	rules->action = NULL;
	rules->interval = DEF_INTERVAL;
	
	if (debug) printf("===== Parse section ================\n");

	for (i=1;i<lines;i+=2)
	{
		char * param = get_first_param((char*)line[i]);
		
		if (debug) printf("Param: <%s>\n",param);
	
		if ( strcasecmp(param,LOGACTION) == 0 )
		{
			set_param_logaction(rules,(char*)line[i]);
		}
		else if ( strcasecmp(param,DEBUG) == 0 )
		{
			rules->debug = 1;
		}
		else if ( strcasecmp(param,INTERVAL) == 0 )
		{
			int params[1];
			int n = exec_pcre((char*)line[i],INTERVAL_PCRE,params,0,0);
			if (n!=2) 
			{ 
				printf("Incorrect string format: <%s>\n",(char*)line[i]);
				exit(1);
			}
			rules->interval = atoi((char*)params[1]); 
		}
		else
		{
			printf("Error: unknown param <%s>\n",param);
			exit(1);
		}
		free(param);
	}

	if (debug) printf("===== end parse section ============\n");

	free_sections(line,lines);
	return rules;
}

char * get_first_param(char * str)
{ // Get first param from string
	int param[1];
	int n = exec_pcre(str,"^\\s*\\S+",param,0,0);
	if (n==0) return NULL;
	return (char*)param[0];
}

char * get_params_string(char * str)
{
	char * p = strchr(str, 32);
	if (p)
	{
		p++; //Skip first space
		int len = strlen(p);
		char * buf = malloc(sizeof(char)*len+1);
		strncpy(buf,p,len);
		buf[len]=0;
		return buf;
	}
	return NULL;
}

void sig_chld(int signo)
{
	int status;
	
	if (waitpid(0, &status, WUNTRACED) < 0)
	{
		//printf("waitpid failed\n");
		return;
	}
}

int read_line(int * log_fd, char * log_buf)
{
	char * buf[1];
	buf[0]='\0';

	int read_bytes=0;
	int n=0;

	log_buf[0]='\0';

	while (1)
	{
		n = read((int)log_fd,buf,1);
		if (n<1 || strncmp(buf,"\n",1)==0) break;
		strncat(log_buf, buf, n);
		read_bytes+=n;
	} 
	log_buf[read_bytes]='\0';

	//if (n) printf("%d: [%s]\n",read_bytes,log_buf);

	return read_bytes;
}

pcre * compile_pcre(char * regexp)
{
	const char *errstr;
	int errchar; 

	pcre * f;
	
	if( (f=pcre_compile(regexp,0,&errstr,&errchar,NULL) )==NULL)
	{
		fprintf(stderr,"Error: %s\nSymbol: %i\nPattern: %s\n",errstr,errchar,regexp);
		exit(1);
	}
	return f;
}

int exec_pcre(char * string, char * regexp, int * array, pcre * f, pcre_extra * f_ext)
{
	int vector[128];
	int vecsize=128; 
	int pairs;
	int found_params=0;

	int offset=0;
	int string_len=strlen(string);

	// If not defined f
	if (f==0) f = compile_pcre(regexp);
	
	while ((pairs=pcre_exec(f,f_ext,string,string_len,offset,PCRE_NOTEMPTY,
		vector,vecsize))>0)
	{
		if (debug) printf("====== Exec_pcre=====\n");
		
		offset = vector[1];
		
		char * data;
		int i;
		int len;
		
		for (i=0;i<(pairs*2);i+=2)
		{
			len = vector[i+1] - vector[i];
			data = (char*) malloc(sizeof(char)*len+1);
			memcpy(data,string + vector[i],len);
			data[len]='\0';
			array[found_params] = (int)data;
			found_params++;
			if (debug) printf("<%s>\n",data);
		}
		if (debug) printf("======================\n");
	}		
	return found_params;
}

void free_sections(int * sections, int count)
{
	int i;
	for(i=0;i<count;i++)
	{
 		if (debug) printf("Free: <%s>\n",(char*)sections[i]);
		free((char*)sections[i]);
	}
}

void set_param_logaction(struct struct_rules * rules, char * line)
{
	int value[6];
	int count = exec_pcre(line,LOGACTION_PCRE,value,0,0);

	if (count<2)
	{
		fprintf(stderr,"Incorrect format string: <%s>\n",line);
		exit(1);
	}

	char * regexp = (char*)value[1];
	char * file_string = (char*)value[2];
	const char *errstr;		
	pcre * f = compile_pcre(regexp);
	pcre_extra * pe  = pcre_study(f,0,&errstr);
	struct struct_action * action; 
	action = malloc(sizeof(struct struct_action));		
	action->regexp = f;
	action->regexp_ext = pe;
	action->file = get_first_param(file_string);
	action->params = get_params_string(file_string);

	if (count == 6) 
	{
		action->max_matchers = atoi((char*)value[4]); 
		action->time_matcher = atoi((char*)value[5]);
	}
	else 
	{
		action->max_matchers=1;
		action->time_matcher=0;
	}

	action->next = NULL;
	action->matcher = NULL;	

	if (rules->action == NULL)
	{ //For first action
		rules->action = action;
	}
	else
	{ // Search last action and set pointer to NEW action
		struct struct_action * sub_action = rules->action;
		while (sub_action->next != NULL) sub_action = sub_action->next;
		sub_action->next = action;
	}
	// Last element already free? (file_string)
	free_sections(value,count);
}

void start_logaction(struct struct_rules * logaction)
{
	if (!debug)
	{

		int p = fork();
		if (p != 0) return; //I'am parent

	}

	int * log_fd = (int *) malloc(sizeof(int));

	//if ((log_fd = fopen(logaction->log_file_name, "r")) == NULL) exit(1);

	log_fd = open(logaction->log_file_name,O_RDONLY);	
	if (log_fd<0)
	{
		fprintf(stderr,"Can't open log file: %s:\n",logaction->log_file_name);
		perror(NULL);
		exit(1);
	}

	// Go to end file
	lseek( (int) log_fd, 0, SEEK_END);

	char file_string[MAX_FILE_STRING_LENGTH];
			
	for  ( ; ; )
	{	
		while (read_line( (int *) log_fd,file_string)>0) match_pcre(logaction->action,file_string);
		sleep(logaction->interval);
	}	
	close(log_fd);
}

void match_pcre (struct struct_action * action, char * str)
{	
	while (action != NULL)
	{
		int values[MAX_PARAMS_COUNT];
		int n = exec_pcre(str,NULL,values,action->regexp,action->regexp_ext);	
	
		if (n>0)
		{
			if (debug) printf("___MATCH_RULE___\n");

			// Start action file
			char * new_argv[MAX_PARAMS_COUNT+2];	
			new_argv[0] = action->file;
			new_argv[1] = NULL;
			int i;				

			// Parse params	
			if (action->params)
			{
				int params[MAX_PARAMS_COUNT];
				int params_count = exec_pcre(action->params,"\\S+",params,0,0);
							
				for (i=0;i<params_count;i++)
				{			
					// Make $n values
					int c = strncmp((char*)params[i],"$",1);
					if (c==0)
					{
						c = atoi((char*)params[i]+1);
						if (c<n) params[i] = values[c];	
					}
					new_argv[i+1] = (char*)params[i];
					if (debug) printf("Exec param %d: <%s>\n",
						i+1,(char*)params[i]);
				}

				new_argv[params_count+1]=NULL;
			}

			// Make values string for matcher
			char values_string[64]; values_string[0] = '\0';
			for (i=1;i<n;i++)
			{
				strcat(values_string, (char*)values[i]);
				strcat(values_string, ".");
			}

			if ( add_matcher(action, values_string) )
			{
				// Exec file
				if (debug) printf("Exec: <%s>\n",new_argv[0]);

				pid_t p = fork();
				if (p==0)
				{
					execv(new_argv[0], new_argv);
				}
				else if (p<0)	if (debug) fprintf(stderr,"Can't fork\n");
			}
		}
		action = action->next;
	}	
}

int add_matcher(struct struct_action * action, char * str)
{
	struct matcher * matcher;

	if (action->max_matchers < 2)
	{
		return 1;
	}
	
	if (action->matcher == NULL)
	{ // Create first matcher
		matcher = malloc(sizeof(struct matcher));	
		matcher->cur_count = 1;
		matcher->str = strdup(str);
		matcher->next = NULL;	
		matcher->time = (long int) time(NULL); 
		action->matcher = matcher;
		return 0;
	}
	
	matcher = action->matcher;
	
	for (;;)	
        {
		if ( strcmp(matcher->str,str)==0 )
		{ // Found existed matcher
			if ( (action->time_matcher > 0) && ((time(NULL)-matcher->time) > action->time_matcher) ) 
			{ 
				matcher->time = (long int) time(NULL); 
				matcher->cur_count = 0; 

				if (debug) fprintf(stderr,"TIME: RESET\n");
			}

			if (debug) fprintf(stderr,"TIME: %d,%d\n",time(NULL)-matcher->time,action->time_matcher);
			
			matcher->cur_count += 1;

			if (debug) fprintf(stderr,"FOUND %s [%d/\%d]\n",(char*)str, matcher->cur_count, action->max_matchers);

			if (matcher->cur_count >= action->max_matchers)
			{
				//struct matcher * old_matcher = matcher;				
				//matcher = matcher->next;
				//free(old_matcher);				
				matcher->cur_count = 0;
				
				if (debug) fprintf(stderr,"MATCHER FOUND AND MUST BEEN EXECUTED\n");

				return 1;
			}
			return 0;
		}

		if (matcher->next == NULL) break;
		matcher	= matcher->next;
	}

	// matcher not found. Create new (next)
	matcher->next = malloc(sizeof(struct matcher));	
	matcher = matcher->next;
	matcher->cur_count = 1;
	matcher->str = strdup(str);
	matcher->next = NULL;
	matcher->time = (long int) time(NULL); 
	
	return 0;
}


