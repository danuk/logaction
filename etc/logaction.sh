#!/bin/bash

# See for details: https://gitlab.com/danuk/logaction

working_dir=/opt/logaction
logaction=/opt/logaction/bin/logaction
config=/opt/logaction/conf/logaction.cfg

case "$1" in
        start)
		cd $working_dir
		$logaction -c $config
                exit 0;
        ;;
        stop)
		killall logaction
                exit 0;
	;;
	restart)
		killall logaction
		$logaction -c $config
		exit 0;
	;;
	*)
                echo "Usage: $0 [start|stop|restart]"
        ;;
esac
exit 1;


